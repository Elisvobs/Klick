package com.elisvobs.klick;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.elisvobs.klick.fragment.health.AlertsActivity;
import com.elisvobs.klick.fragment.health.AlertsFragment;
import com.elisvobs.klick.fragment.health.AmbulanceActivity;
import com.elisvobs.klick.fragment.health.AmbulanceFragment;

public class HealthActivity extends AppCompatActivity {

    public Fragment fragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();

            if (id == R.id.navigation_notifications) {
//                fragment = new AlertsFragment();
                startActivity(new Intent(HealthActivity.this, AlertsActivity.class));
//                setTitle(R.string.alerts);
            } else if (id == R.id.navigation_dashboard) {
//                fragment = new AmbulanceFragment();
                startActivity(new Intent(HealthActivity.this, AmbulanceActivity.class));
//                setTitle(R.string.ambulance_assistance);
            }

//            FragmentManager fragmentManager = getSupportFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.main_container, fragment).addToBackStack(null).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health);
        setTitle(R.string.health);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}