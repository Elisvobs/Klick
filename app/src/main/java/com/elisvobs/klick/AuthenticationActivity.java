package com.elisvobs.klick;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthenticationActivity extends AppCompatActivity {
    @BindView(R.id.phoneNumber) TextInputEditText phoneNumber;
    @BindView(R.id.verificationed) TextInputEditText code;
    @BindView(R.id.sendverifybt) Button button;
    @BindView(R.id.timertv) TextView timerText;
    @BindView(R.id.verifiedSign) ImageView verify;

    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private Timer timer;
    private Boolean mVerified = false;
    private String mVerificationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mAuth = FirebaseAuth.getInstance();
//        if (mAuth.getCurrentUser() != null) {
//            startActivity(new Intent(AuthenticationActivity.this, MainActivity.class));
//            finish();
//        }

        setContentView(R.layout.activity_authentication);
        setTitle(R.string.login);
        ButterKnife.bind(this);

        FirebaseApp.initializeApp(this);
        //Get Firebase auth instance
        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d("TAG", "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w("TAG", "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Snackbar.make(findViewById(R.id.parentLayout),
                            R.string.invalid_code, Snackbar.LENGTH_LONG).show();
                }
                else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(R.id.parentLayout),
                            R.string.many_requests, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                Log.d("TAG", "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button.getTag().equals(getResources().getString(R.string.tag_send))) {
                    if (!phoneNumber.getEditableText().toString().trim().isEmpty()
                            && phoneNumber.getEditableText().toString().trim().length() >= 10) {
                        startPhoneNumberVerification(phoneNumber.getEditableText().toString().trim());
                        mVerified = false;
                        startTimer();
                        code.setVisibility(View.VISIBLE);
                        button.setTag(getResources().getString(R.string.tag_verify));
                    } else {
                        phoneNumber.setError("Please enter valid mobile number");
                    }
                }

                if (button.getTag().equals(getResources().getString(R.string.tag_verify))) {
                    if (!code.getEditableText().toString().trim().isEmpty() && !mVerified) {
                        Snackbar.make(findViewById(R.id.parentLayout), R.string.verification_status,
                                Snackbar.LENGTH_LONG).show();
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId,
                                code.getEditableText().toString().trim());
                        signInWithPhoneAuthCredential(credential);
                    }
                    if (mVerified) {
//                        startActivity(new Intent(RegisterActivity.this, SuccessActivity.class));
                        finish();
                    }
                }
            }
        });

        timerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!phoneNumber.getEditableText().toString().trim().isEmpty()
                        && phoneNumber.getEditableText().toString().trim().length() == 10) {
                    resendVerificationCode(phoneNumber.getEditableText().toString().trim(), mResendToken);
                    mVerified = false;
                    startTimer();
                    code.setVisibility(View.VISIBLE);
                    button.setTag(getResources().getString(R.string.tag_verify));
                    Snackbar.make(findViewById(R.id.parentLayout), R.string.code_resend,
                            Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            mVerified = true;
                            timer.cancel();
                            verify.setVisibility(View.VISIBLE);
                            timerText.setVisibility(View.INVISIBLE);
                            phoneNumber.setEnabled(false);
                            code.setVisibility(View.INVISIBLE);
                            Snackbar.make(findViewById(R.id.parentLayout), R.string.code_verified,
                                    Snackbar.LENGTH_LONG).show();
//                            startActivity(new Intent(AuthenticationActivity.this, MainActivity.class));
                        } else {
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            // The verification code entered was invalid
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException)
                                Snackbar.make(findViewById(R.id.parentLayout), R.string.invalid_code,
                                        Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void signOut(){
        mAuth.signOut();
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                30,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    public void startTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {

            int second = 60;
            @Override
            public void run() {
                if (second <= 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timerText.setText(R.string.resend_code);
                            timer.cancel();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timerText.setText("00:" + second--);
                        }
                    });
                }
            }
        }, 0, 1000);
    }

    private void resendVerificationCode(String phoneNumber, PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
}