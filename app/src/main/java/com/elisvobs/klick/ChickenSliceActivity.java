package com.elisvobs.klick;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterViewFlipper;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.elisvobs.klick.adapter.CustomAdapter;

public class ChickenSliceActivity extends AppCompatActivity {
    public static final String[] menu = {"", "One Piecer", "Two Piecer", "Three Piecer", "Half Chicken", "Full Chicken"};
    public static final int [] IMAGES = {R.drawable.two_piecer, R.drawable.three_piecer, R.drawable.family_treat_buckect};
    public static final String [] NAMES = {"2 Piecer", "3 Piecer", "Family Treat"};
    private static final String PREFS_FILE = "PREFS_FILE";
    private static final String KEY_CHECKBOX = "KEY_CHECKBOX";

    AdapterViewFlipper adapterViewFlipper;
    CheckBox checkbox, checkbox1, checkbox2, checkbox3, checkbox4;
    Button button;
//    int price = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chicken_slice);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapterViewFlipper = findViewById(R.id.adapterViewFlipper);
        checkbox = findViewById(R.id.checkbox);
        checkbox1 = findViewById(R.id.checkbox1);
        checkbox2 = findViewById(R.id.checkbox2);
        checkbox3 = findViewById(R.id.checkbox3);
        checkbox4 = findViewById(R.id.checkbox4);
        button = findViewById(R.id.customBtn);


        CustomAdapter adapter = new CustomAdapter(getApplicationContext(), IMAGES, NAMES);
        adapterViewFlipper.setAdapter(adapter);
        adapterViewFlipper.setFlipInterval(2500);
        adapterViewFlipper.setAutoStart(true);

        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.apply();
        Boolean isChecked = sharedPreferences.getBoolean(KEY_CHECKBOX, false);
        checkbox.setChecked(isChecked);
        checkbox1.setChecked(isChecked);
        checkbox2.setChecked(isChecked);
        checkbox3.setChecked(isChecked);
        checkbox4.setChecked(isChecked);
    }

    public void customiseMenu (View view){

        if (checkbox.isChecked() && checkbox1.isChecked() && checkbox2.isChecked() && checkbox3.isChecked() && checkbox4.isChecked()) {
            customiseDialog();
        } else if (checkbox.isChecked() && checkbox1.isChecked() && checkbox2.isChecked() && checkbox3.isChecked()){
            customiseDialog();
        } else  if (checkbox4.isChecked()){
            customiseDialog();
        } else if (checkbox3.isChecked()){
            customiseDialog();
        } else if (checkbox2.isChecked()) {
            customiseDialog();
        } else if (checkbox1.isChecked()){
            customiseDialog();
        } else if (checkbox.isChecked()) {
            customiseDialog();
        } else {
            Toast.makeText(ChickenSliceActivity.this, "Please select a menu", Toast.LENGTH_LONG).show();
        }
    }

    private void customiseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.toppings, null);
        builder.setView(dialogView);

        builder.setTitle(getResources().getString(R.string.toppings))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        checkOut();
                    }
                }).setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                checkOut();
            }
        }).create().show();
    }

    private void checkOut() {
        startActivity(new Intent(this, CheckoutActivity.class));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, FoodActivity.class));
    }

//    public void processOrder(View view) {
//        TextView textView = (TextView) mSpinner.getSelectedView();
//        String menu = textView.getText().toString();
//        if (TextUtils.isEmpty(menu)){
//            Toast.makeText(ChickenSliceActivity.this, "Please select a menu", Toast.LENGTH_LONG).show();
//        } else {
//            Intent intent = new Intent(this, ConfirmationActivity.class);
//            intent.putExtra("menu", menu);
//            startActivity(intent);
//        }
//    }

//    public void addOrder(View view){

//    }

//    private void displayPrice(int amount) {
//        TextView priceTextView = findViewById(R.id.priceText);
//        priceTextView.setText("$" + amount);
//    }
}