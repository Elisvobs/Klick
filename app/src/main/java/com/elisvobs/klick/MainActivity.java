package com.elisvobs.klick;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    @BindView(R.id.accoCard) MaterialCardView accommodation;
    @BindView(R.id.eventCard) MaterialCardView events;
    @BindView(R.id.foodCard) MaterialCardView food;
    @BindView(R.id.groceryCard) MaterialCardView grocery;
    @BindView(R.id.healthCard) MaterialCardView health;
    @BindView(R.id.policeCard) MaterialCardView police;


    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //get firebase auth instance
        auth = FirebaseAuth.getInstance();
        //get current user
//        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//        authListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
//                    startActivity(new Intent(MainActivity.this, AuthenticationActivity.class));
//                    finish();
//                }
//            }
//        };

        accommodation.setOnClickListener(this);
        events.setOnClickListener(this);
        food.setOnClickListener(this);
        grocery.setOnClickListener(this);
        health.setOnClickListener(this);
        police.setOnClickListener(this);
    }

    public void onClick(View view){
        int id = view.getId();
        switch (id){
            case R.id.accoCard:
                startActivity(new Intent(this, AccommodationActivity.class));
                finish();
                break;
            case R.id.eventCard:
                startActivity(new Intent(this, EventsActivity.class));
                finish();
                break;
            case R.id.foodCard:
                startActivity(new Intent(this, FoodActivity.class));
                finish();
                break;
            case R.id.groceryCard:
                startActivity(new Intent(this, GroceryActivity.class));
                finish();
                break;
            case R.id.healthCard:
                startActivity(new Intent(this, HealthActivity.class));
                finish();
                break;
            case R.id.policeCard:
                startActivity(new Intent(this, PoliceActivity.class));
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.activities:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case R.id.language:
                startActivity(new Intent(this, LanguageActivity.class));
                break;
            case R.id.logout:
                signOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void signOut() {
        auth.signOut();
    }

    @Override
    public void onStart() {
        super.onStart();
//        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
//        if (authListener != null) {
//            auth.removeAuthStateListener(authListener);
//        }
    }
}