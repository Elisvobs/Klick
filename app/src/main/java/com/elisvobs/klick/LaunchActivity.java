package com.elisvobs.klick;

import android.content.Intent;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.facebook.shimmer.ShimmerFrameLayout;

public class LaunchActivity extends AppCompatActivity {
    ShimmerFrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch_activity);

        container = findViewById(R.id.shimmer_view);

    }

    public void startKlick(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        container.startShimmer();
    }

    @Override
    protected void onPause() {
        container.stopShimmer();
        super.onPause();
    }
}