package com.elisvobs.klick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        setTitle(R.string.order_summary);

        TextView menu = findViewById(R.id.menuInfo);

        Intent intent = getIntent();
        String menuInfo = intent.getStringExtra("menu");

        menu.setText(menuInfo);
    }
}