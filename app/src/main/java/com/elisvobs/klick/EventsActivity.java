package com.elisvobs.klick;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.elisvobs.klick.adapter.Event;
import com.elisvobs.klick.adapter.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class EventsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    Context context;
    private List<Event> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        setTitle(R.string.events);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        initEvents();
        initAdapter();
    }

    private void initEvents() {
        events = new ArrayList<>();
        events.add(new Event("Google Developers Fest", R.drawable.gdg_event));
        events.add(new Event("Google Developers Fest", R.drawable.gdg_event));
        events.add(new Event("Google Developers Fest", R.drawable.gdg_event));
        events.add(new Event("Google Developers Fest", R.drawable.gdg_event));
        events.add(new Event("Google Developers Fest", R.drawable.gdg_event));
    }

    private void initAdapter() {
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(events);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}