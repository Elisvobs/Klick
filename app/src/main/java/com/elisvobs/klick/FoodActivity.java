package com.elisvobs.klick;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FoodActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.chknInn) MaterialCardView chickenInn;
    @BindView(R.id.chknSlice) MaterialCardView chickenSlice;
    @BindView(R.id.eatLick) MaterialCardView eatLick;
    @BindView(R.id.kfc) MaterialCardView kfc;
    @BindView(R.id.nandos) MaterialCardView nandos;
    @BindView(R.id.pizzaInn) MaterialCardView pizzaInn;
    @BindView(R.id.pizzaSlice) MaterialCardView pizzaSlice;
    @BindView(R.id.steers) MaterialCardView steers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        setTitle(R.string.food);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        chickenInn.setOnClickListener(this);
        chickenSlice.setOnClickListener(this);
        eatLick.setOnClickListener(this);
        kfc.setOnClickListener(this);
        nandos.setOnClickListener(this);
        pizzaInn.setOnClickListener(this);
        pizzaSlice.setOnClickListener(this);
        steers.setOnClickListener(this);
    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.chknInn:
                startActivity(new Intent(this, ChickenInnActivity.class));
                break;
            case R.id.chknSlice:
                startActivity(new Intent(this, ChickenSliceActivity.class));
                finish();
                break;
            case R.id.eatLick:
                startActivity(new Intent(this, EatnLickActivity.class));
                break;
            case R.id.kfc:
                startActivity(new Intent(this, KfcActivity.class));
                break;
            case R.id.nandos:
                startActivity(new Intent(this, NandosActivity.class));
                break;
            case R.id.pizzaInn:
                startActivity(new Intent(this, PizzaInnActivity.class));
                break;
            case R.id.pizzaSlice:
                startActivity(new Intent(this, PizzaSliceActivity.class));
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}