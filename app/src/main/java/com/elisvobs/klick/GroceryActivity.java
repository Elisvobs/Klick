package com.elisvobs.klick;

import android.content.Intent;
import android.support.design.card.MaterialCardView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GroceryActivity extends AppCompatActivity {
    MaterialCardView foodWorld, choppies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery);
        setTitle(R.string.grocery);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        foodWorld = findViewById(R.id.foodWorld);
        choppies = findViewById(R.id.choppies);

        foodWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GroceryActivity.this, FoodWorldActivity.class));
            }
        });

        choppies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GroceryActivity.this, ChoppiesActivity.class));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}