package com.elisvobs.klick;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.elisvobs.klick.adapter.CheckoutViewAdapter;
import com.elisvobs.klick.adapter.Order;

import java.util.List;


public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String PREFS_FILE = "PREFS_FILE";
    private static final String KEY_CHECKBOX = "KEY_CHECKBOX";
    Button backBtn, checkOut;
    CheckBox cheeseBox, mushBox, saladBox;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int quantity = 0;

//    RecyclerView recyclerView;
//    List<Order> order;
//    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

//        recyclerView = findViewById(R.id.orderList);
//        recyclerView.setHasFixedSize(true);

//        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
//        recyclerView.setLayoutManager(layoutManager);
//        CheckoutViewAdapter adapter = new CheckoutViewAdapter(order);
//        recyclerView.setAdapter(adapter);


        cheeseBox = findViewById(R.id.cheeseBox);
        mushBox = findViewById(R.id.mushBox);
        saladBox = findViewById(R.id.saladBox);
        backBtn = findViewById(R.id.backBtn);
        checkOut = findViewById(R.id.checkoutBtn);
        checkOut.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        sharedPreferences = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();

        Boolean isChecked = sharedPreferences.getBoolean(KEY_CHECKBOX, false);
        cheeseBox.setChecked(isChecked);
        mushBox.setChecked(isChecked);
        saladBox.setChecked(isChecked);
    }


    /**
     * This method is called when the plus button is clicked.
     */
    public void increment(View view) {
        if (quantity == 100) {
            return;
        }
        quantity = quantity + 1;
        displayQuantity(quantity);
        // arithmetic method
    }

    /**
     *
     * This method is called when the minus button is clicked.
     */
    public void decrement(View view) {
        if (quantity == 0 ) {
            return;
        }
        quantity = quantity - 1;
////        price -= price;
        displayQuantity(quantity);
////        displayPrice(price);
    }

    private void displayQuantity(int number) {
        TextView quantityTextView = findViewById(R.id.orderQuantity);
        quantityTextView.setText("" + number);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
//        if (id == R.id.checkoutBtn) {
//            checkOutDialog();
//        } else {
//            onBackPressed();
//        }
    }

    private void checkOutDialog() {
//        FullScreenDialog dialog = new FullScreenDialog();
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        dialog.show(ft, FullScreenDialog.TAG);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.checkout_add, null);
        builder.setView(dialogView);

        builder.setTitle(getResources().getString(R.string.order_summary)).create().show();
//        Button delivery, order;
//        delivery= findViewById(R.id.delivery);
//        order = findViewById(R.id.order);
//        delivery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ChickenSliceActivity.class));
    }
}