package com.elisvobs.klick.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.elisvobs.klick.R;

import java.util.List;

public class CheckoutViewAdapter extends RecyclerView.Adapter<CheckoutViewAdapter.CheckoutViewHolder> {
    List<Order> orders;

    public CheckoutViewAdapter(List<Order> orders) {
        this.orders = orders;
    }

    class CheckoutViewHolder extends RecyclerView.ViewHolder{
        Button minus, plus;
        CheckBox cheeseBox, mushBox, saladBox;
        TextView orderName, price, quantity;
        ImageButton remove;

        public CheckoutViewHolder(@NonNull View itemView) {
            super(itemView);
            cheeseBox = itemView.findViewById(R.id.cheeseBox);
            mushBox = itemView.findViewById(R.id.mushBox);
            saladBox = itemView.findViewById(R.id.saladBox);
            orderName = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            quantity = itemView.findViewById(R.id.orderQuantity);
            remove = itemView.findViewById(R.id.remove);
        }
    }

    @NonNull
    @Override
    public CheckoutViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_order, parent, false);
        return new CheckoutViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckoutViewHolder checkoutViewHolder, int i) {

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}