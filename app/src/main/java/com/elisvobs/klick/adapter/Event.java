package com.elisvobs.klick.adapter;

public class Event {
    String eventName;
    int eventId;

    public Event(String eventName, int eventId) {
        this.eventName = eventName;
        this.eventId = eventId;
    }
}