package com.elisvobs.klick.adapter;

public class Order {
    String orderName;
    int price;

    /**
     *
     * @param orderName
     * @param price
     */
    public Order(String orderName, int price) {
        this.orderName = orderName;
        this.price = price;
    }
}