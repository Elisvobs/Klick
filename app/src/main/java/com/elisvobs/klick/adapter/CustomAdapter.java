package com.elisvobs.klick.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.elisvobs.klick.R;

public class CustomAdapter extends BaseAdapter {
    Context context;
    int[] images;
    String[] names;
    LayoutInflater inflater;


    /**
     *
     * @param applicationContext
     * @param images pictures to display
     * @param names names of the items
     */
    public CustomAdapter(Context applicationContext, int[] images, String[] names) {
        this.context = applicationContext;
        this.images = images;
        this.names = names;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.list_item, null);
        TextView name = view.findViewById(R.id.name);
        ImageView image = view.findViewById(R.id.image);
        name.setText(names[position]);
        image.setImageResource(images[position]);
        return view;
    }
}