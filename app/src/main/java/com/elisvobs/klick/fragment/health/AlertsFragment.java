package com.elisvobs.klick.fragment.health;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterViewFlipper;

import com.elisvobs.klick.adapter.CustomAdapter;
import com.elisvobs.klick.R;

public class AlertsFragment extends Fragment {

    public AlertsFragment() {
    }

    int [] ALERTS_IMG = {R.drawable.breast_cancer, R.drawable.cholera_alert, R.drawable.malaria_alert, R.drawable.breast_cancer_alt, R.drawable.typhoid_alert};
    String[] ALERTS = {"Breast Cancer", "Cholera", "Malaria", "Breast Cancer", "Typhoid"};
    AdapterViewFlipper healthAlerts;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alerts, container, false);
        setRetainInstance(true);
        healthAlerts = view.findViewById(R.id.alertsFlipper);

        CustomAdapter adapter = new CustomAdapter(getContext(), ALERTS_IMG, ALERTS);
        healthAlerts.setAdapter(adapter);
        healthAlerts.setFlipInterval(3500);
        healthAlerts.setAutoStart(true);
        return view;
    }
}