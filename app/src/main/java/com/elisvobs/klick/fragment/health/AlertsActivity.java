package com.elisvobs.klick.fragment.health;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterViewFlipper;

import com.elisvobs.klick.R;
import com.elisvobs.klick.adapter.CustomAdapter;

public class AlertsActivity extends AppCompatActivity {

    public static final int [] ALERTS_IMG = {R.drawable.breast_cancer, R.drawable.cholera_alert, R.drawable.malaria_alert, R.drawable.breast_cancer_alt, R.drawable.typhoid_alert};
    public static final String[] ALERTS = {"Breast Cancer", "Cholera", "Malaria", "Breast Cancer", "Typhoid"};
    AdapterViewFlipper healthAlerts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_alerts);

        healthAlerts = findViewById(R.id.alertsFlipper);

        CustomAdapter adapter = new CustomAdapter(AlertsActivity.this, ALERTS_IMG, ALERTS);
        healthAlerts.setAdapter(adapter);
        healthAlerts.setFlipInterval(3500);
        healthAlerts.setAutoStart(true);
    }
}