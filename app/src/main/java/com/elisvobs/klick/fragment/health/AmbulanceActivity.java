package com.elisvobs.klick.fragment.health;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.elisvobs.klick.R;

public class AmbulanceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ambulance);
        setTitle(R.string.ambulance_assistance);

    }

    public void bmc(View view) {
        String number = "+263 8644218830";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)){
            return;
        }
        startActivity(callIntent);
    }

    public void cimasAir(View view) {
        String number = "+263 4 700085";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void cimasRes(View view) {
        String number = "+263 4 700070";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void cityHre(View view) {
        String number = "+263 4 772375";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void emergencyRoom(View view) {
        String number = "+263 4 708883";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void emrasAlert(View view) {
        String number = "+263 4 250012";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
//               public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                                      int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    public void hmmasNetstar(View view) {
        String number = "+263 4 797589";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void mars(View view) {
        String number = "+263 4 771221";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void netStar(View view) {
        String number = "+263 4 797588;";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }

    public void stJohns(View view) {
        String number = "+263 4 797588;;";
        Uri call = Uri.parse("tel:" + number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
                AmbulanceActivity.this, Manifest.permission.CALL_PHONE)) {
            return;
        }
        startActivity(callIntent);
    }
}