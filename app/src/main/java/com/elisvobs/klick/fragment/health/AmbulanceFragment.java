package com.elisvobs.klick.fragment.health;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.elisvobs.klick.R;

public class AmbulanceFragment extends Fragment  {

    public AmbulanceFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ambulance, container, false);
        setRetainInstance(true);
//        Button cimas, emras;
//        cimas = view.findViewById(R.id.btn2);
//        emras = view.findViewById(R.id.btn1);

//        cimas.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String number = "+263 4 700070";
//                Uri call = Uri.parse("tel:" + number);
//                Intent callIntent = new Intent(Intent.ACTION_DIAL, call);
//                if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)) {
//                    return;
//                }
//                startActivity(callIntent);
//            }
//        });
//
//        emras.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String number = "+263 4 250012";
//                Uri call = Uri.parse("tel:" + number);
//                Intent callIntent = new Intent(Intent.ACTION_CALL_BUTTON, call);
//                if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)) {
//                    return;
//                }
//                startActivity(callIntent);
//            }
//        });
        return view;
    }

    public void callAmbulance() {
        String number = "263777175291";
        Uri call = Uri.parse("tel:" + number);
        Intent phoneIntent = new Intent(Intent.ACTION_CALL, call);
        phoneIntent.setAction(Intent.ACTION_CALL);
    }

//    @Override
//    public void onClick(View view) {
//        int id = view.getId();
//        switch (id){
//            case R.id.btn1:
//                Intent phoneIntent = new Intent();
//                phoneIntent.setAction(Intent.ACTION_CALL);
//                break;
//            case R.id.btn2:
//                callAmbulance();
//                break;
//        }
//    }
}